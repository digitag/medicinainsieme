<!DOCTYPE html>
<html>
	<head>
		<title>Card Salvavita MedicinaInsieme</title>
        <?php include('layout/head.php'); ?>
	</head>
<body>
        <?php include('layout/header.php'); ?>
		

		<!-- Blog -->
	    <section id="blog" class="blog section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-8">
						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
							<!-- Post Title -->
							<h2 class="wow fadeInUp">Card Salvavita</h2>
							<div class="post wow fadeInUp">
								<!-- Image -->
								<div class="post-content">	
									<!-- Text -->
									<p>La <strong>Card Salvavita</strong> di <strong>MedicinaInsieme</strong> è l'unico servizio in Italia che ti garantisce un'assistenza rapida ed efficace.<br>
In caso di <strong>emergenza</strong>, scansionando il QR code presente sulla card, si potrà accedere a tutte le informazioni utili per un intervento tempestivo, che tenga presente dei potenziali fattori di rischio.</p>
<p>La Card contine i seguenti dati utili:</p>
<ul>
<li>Generalità</li>
<li>Grupppo sanguigno</li>
<li>Allergie a farmaci</li>
<li>Patologie diagnosticate</li>
</ul>
								</div>
							</div>
						</div><!-- End Post -->
						
	
    
    
    
    
    
    						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
							<!-- Post Title -->
							<h2 class="wow fadeInUp">Attiva la tua Card!</h2>
							<div class="post wow fadeInUp">
								<div class="post-content">	
									<!-- Text -->
									<p>L'attivazione del servizio è semplice e veloce.</p>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img class="pull-left img-responsive" src="images/compra.jpg" alt="Acquista la card Salvavita" style="margin:0 20px 20px 0">
                                        </div>
                                        <div class="col-md-8">
                                            <p><span class="number orange">1</span> Accedi al sito di MedicinaInsieme ed <strong>acquista online la Card Salvavita</strong>.</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img class="pull-left img-responsive" src="images/compila.jpg" alt="Compila la card Salvavita" style="margin:0 20px 20px 0">
                                        </div>
                                        <div class="col-md-8">
                                            <p><span class="number green">2</span> <strong>Compila la tua area personale</strong> con i tuoi dati. Chiedi al tuo medico di MedicinaInsieme di aiutarti a completare la compilazione.</p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <img class="pull-left img-responsive" src="images/porta.jpg" alt="Porta con te la card Salvavita" style="margin:0 20px 20px 0">
                                        </div>
                                        <div class="col-md-8">
                                            <p><span class="number yellow">3</span> <strong>Porta sempre con te la Card Salvavita</strong> che riceverai direttamente a casa tua.</p>
                                        </div>
                                    </div>
								</div>
							</div>
						</div><!-- End Post -->

    
    
    
    
    
    
    
    
    
    
    					
						
					</div><!-- Blog Left Side Ends -->
					
					
					<!-- Blog Sidebar Begins -->
					<div class="col-md-4">
					
						<div class="sidebar wow fadeInRight" data-animation="fadeInUp" data-animation-delay="300">
                            <button class="btn">
                            <a href="acquista-card.php">Acquista ora</a>
                            </button>							
						</div>
						
					</div><!-- Blog Sidebar Ends -->
					
				</div>
			
			</div>
		</section><!-- Our Blog Section Ends -->
		

        <?php include('layout/footer.php'); ?>
		
	</body>
</html>