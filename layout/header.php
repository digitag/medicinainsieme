		<header id="header" class="clearfix">
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="row">
						<!-- logo -->
						<div class="col-sm-3">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a href="index.php" class="navbar-brand"><img src="images/logo.png" alt="logo">
								</a>

							</div>
						</div><!-- logo -->

						<!-- navbar -->
						<div class="col-sm-9 np">
							<div class="navbar-collapse collapse">
								<ul class="nav navbar-nav">
									<li class="scroll active"><a href="index.php">home</a></li>
									<li class="scroll"><a href="chi-siamo.php">Chi siamo</a></li>
									<li class="scroll"><a href="index.php#services-offered">Servizi</a></li>
									<li class="scroll"><a href="index.php#how-to-reach-us">Contattaci</a></li>
									<li class="scroll"><a href="login.php">Login</a></li>
								</ul>
							</div>
						</div><!-- /navbar -->
					</div>

				</div>
			</div>
		</header>