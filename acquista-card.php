
<!DOCTYPE html>
<html>
	<head>
        <?php include('layout/head.php'); ?>
	<style>
	#contact-form input, #contact-form select, #contact-form textarea {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.2);}
	#contact-form input:hover, #contact-form select:hover, #contact-form textarea:hover {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.4);}
	#referti a{display:block;color:#333;text-decoration:underline}
	#referti a:hover{color:#777;text-decoration:none}
    </style>
	</head>
<body>
        <?php include('layout/header.php'); ?>
		

		<!-- Blog -->
	    <section id="blog" class="blog section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-12">
						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
							<!-- Post Title -->
							<h1 class="wow fadeInUp">Acquista la tua card</h1>
							
                                    		<p>Inserisci i tuoi dati e attendi la nostra email per procedere con l'acquisto della tua card.</p>
							<div class="post wow fadeInUp">
								<div class="post-content">
							<h2 class="wow fadeInUp">Dati anagrafici</h2>

                                    
                                    <form id="contact-form" class="contact-form" name="contact-form" method="" action="">
                        <h1 class="show-on-success lead" style="display:none;">Thanks for the mail, We will contact you shortly</h1>
							<div class="row">
								<div class="form-group col-sm-6 name-field">
                                <label for="exampleInputEmail1">Nome</label>
					                <input type="text" id="name" name="name" class="form-control" required placeholder="Inserisci il tuo nome">
					            </div>
                                <div class="form-group col-sm-6 email-field">
                                <label for="exampleInputEmail1">Cognome</label>
					                <input type="text" id="surname" name="surname" class="form-control" required placeholder="Inserisci il tuo Cognome">
					            </div>
                            
                                <div class="form-group col-sm-6 email-field">
                                <label for="exampleInputEmail1">Indirizzo Email</label>
					                <input type="email" id="email" name="email" class="form-control" required placeholder="Inserisci il tuo indirizzo email">
					            </div>
								
<div class="clearfix"></div>
			            <div class="form-group">
       						<button type="submit" class="btn wow fadeInRight">Invia</button>
				            </div>
				        </form>
								</div>
							</div>
						</div><!-- End Post -->
						

						
                        
                        
                    

							
				</div>
			
			</div>
		</section>
		

        <?php include('layout/footer.php'); ?>
		
	</body>
</html>