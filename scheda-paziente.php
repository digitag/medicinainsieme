
<!DOCTYPE html>
<html>
	<head>
		<title>Scheda paziente MedicinaInsieme</title>
        <?php include('layout/head.php'); ?>
	<style>
	#contact-form input, #contact-form select, #contact-form textarea {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.2);}
	#contact-form input:hover, #contact-form select:hover, #contact-form textarea:hover {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.4);}
	#referti a{display:block;color:#333;text-decoration:underline}
	#referti a:hover{color:#777;text-decoration:none}
    </style>
	</head>
<body>
        <?php include('layout/header.php'); ?>

		<!-- Blog -->
	    <section id="blog" class="blog section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-12">
						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
							<!-- Post Title -->
							<h1 class="wow fadeInUp">Francesco Verdi</h1>
							<div class="post wow fadeInUp">
								<div class="post-content">
							<h2 class="wow fadeInUp">Dati anagrafici</h2>
									<!-- Text -->
                                    <div class="row">
                                        <div class="col-md-6">
                                        <p><strong>NOME</strong> Francesco</p>
                                        <p><strong>COGNOME</strong> Verdi</p>
                                        </div>
                                        <div class="col-md-6">
                                        <p><strong>DATA DI NASCITA</strong> 27/12/1987</p>
                                        <p><strong>CODICE FISCALE</strong> VRDFNC87N27F205L</p>
                                        </div>
                                        <div class="col-md-6">
                                        <p><strong>SESSO</strong> Uomo</p>
                                        </div>
                                    </div>
                                    
									<p>Clicca sulla voce per modificarla.</p>
                                    <form id="contact-form" class="contact-form" name="contact-form" method="" action="">
                        <h1 class="show-on-success lead" style="display:none;">Thanks for the mail, We will contact you shortly</h1>
							<div class="row">
								<div class="form-group col-sm-6 name-field">
                                <label for="exampleInputEmail1">Indirizzo</label>
					                <input type="text" id="name" name="name" class="form-control" required placeholder="Via De Amicis, 30">
					            </div>
                                <div class="form-group col-sm-6 email-field">
                                <label for="exampleInputEmail1">Citt&aacute;</label>
					                <input type="email" id="email" name="email" class="form-control" required placeholder="Concorezzo">
					            </div>
                                <div class="form-group col-sm-6 name-field">
                                <label for="exampleInputEmail1">Provincia</label>
					                <input type="text" id="name" name="name" class="form-control" required placeholder="MB">
					            </div>
                                <div class="form-group col-sm-6 email-field">
                                <label for="exampleInputEmail1">CAP</label>
					                <input type="email" id="email" name="email" class="form-control" required placeholder="20049">
					            </div>

                                <div class="form-group col-sm-6 email-field">
                                <label for="exampleInputEmail1">Telefono</label>
					                <input type="email" id="email" name="email" class="form-control" required placeholder="012 345 678">
					            </div>

                                <div class="form-group col-sm-6 email-field">
                                <label for="exampleInputEmail1">Indirizzo Email</label>
					                <input type="email" id="email" name="email" class="form-control" required placeholder="mario.rossi@email.it">
					            </div>
								<div class="form-group col-sm-6 name-field">
                                <label for="exampleInputEmail1">Password</label>
					                <input type="text" id="name" name="name" class="form-control" required placeholder="*******">
					            </div>
<div class="clearfix"></div>
			            <div class="form-group">
       						<button type="submit" class="btn wow fadeInRight">Salva</button>
				            </div>
				        </form>
								</div>
							</div>
						</div><!-- End Post -->
						
						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp"  data-animation-delay="300">
							<!-- Post Title -->
							<h2 class="wow fadeInUp">Cartella medica</h2>
							<div class="post wow fadeInUp">
								<div class="post-content">	
									<!-- Text -->
									<p>Clicca sulla voce per modificarla.</p>
                                  <form id="contact-form" class="contact-form" name="contact-form" method="" action="">
                        <h1 class="show-on-success lead" style="display:none;">Thanks for the mail, We will contact you shortly</h1>
							<div class="row">
								<div class="form-group col-sm-6 name-field">
                                <label for="exampleInputEmail1">Allergie</label>
					                <input type="text" id="name" name="name" class="form-control" required placeholder="Graminacee">
					            </div>
                                <div class="form-group col-sm-6 email-field">
                                <label for="exampleInputEmail1">Intolleranze</label>
					                <input type="email" id="email" name="email" class="form-control" required placeholder="Glutine, latticini">
					            </div>
								<div class="form-group col-sm-6 name-field">
                                <label for="exampleInputEmail1">Anamnesi</label>
					                <input type="text" id="name" name="name" class="form-control" required placeholder="Anemia mediterranea (madre)">
					            </div>
								<div class="form-group col-sm-6 name-field">
                                <label for="exampleInputEmail1">Patologie diagnosticate</label>
					                <input type="text" id="name" name="name" class="form-control" required placeholder="Asma">
					            </div>
								<div class="form-group col-sm-6 name-field">
                                <label for="exampleInputEmail1">Terapie in corso</label>
					                <input type="text" id="name" name="name" class="form-control" required placeholder="-">
					            </div>
<div class="clearfix"></div>                                

			            <div class="form-group">
       						<button type="submit" class="btn wow fadeInRight">Salva</button>
				            </div>
				        </form> 
								</div>
							</div>
						</div><!-- End Post -->
						
                        
                        
<div class="post-item wow" data-animation="fadeInUp"  data-animation-delay="300">
							<!-- Post Title -->
							<h2 class="wow fadeInUp">Referti</h2>
							<div class="post wow fadeInUp">
								<div class="post-content">	
									<!-- Text -->
                                  <form id="contact-form" class="contact-form" name="contact-form" method="" action="">
                        <h1 class="show-on-success lead" style="display:none;">Thanks for the mail, We will contact you shortly</h1>
							<div class="row">
                                <div class="form-group col-sm-6 name-field">
                                <label for="exampleInputFile">Carica nuovo referto</label>
                                <input type="file" id="exampleInputFile" style="background-color:#fff;padding:0;height:50px;color:#777">
                              </div>
<div class="clearfix"></div>


			            <div class="form-group">
       						<button type="submit" class="btn wow fadeInRight">Salva</button>
				            </div>
				        </form>         

							</div>
						</div> 
   </div>                     
<div class="post-item wow" data-animation="fadeInUp"  data-animation-delay="300">
							<!-- Post Title -->
							<h2 class="wow fadeInUp">Storico referti</h2>
							<div class="post wow fadeInUp">
								<div class="post-content">	
									<!-- Text -->
							<div class="row" id="referti">
                                <div class="form-group col-sm-6 name-field">
<a href="#">Esami sangue 12/05/2015</a>
<a href="#">Visita oculistica 30/03/2015</a>
<a href="#">ECG 07/01/2015</a>
<a href="#">TAC 15/10/2014</a>

                              </div>
								</div>
							</div>
						</div>                                             
                        
					</div><!-- Blog Left Side Ends -->
							
				</div>
			
			</div>
		</section><!-- Our Blog Section Ends -->
		

        <?php include('layout/footer.php'); ?>
		
	</body>
</html>