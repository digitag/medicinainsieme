
<!DOCTYPE html>
<html>
	<head>
		<title>Chi siamo MedicinaInsieme</title>
        <?php include('layout/head.php'); ?>
	<style>
	#contact-form input, #contact-form select, #contact-form textarea {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.2);}
	#contact-form input:hover, #contact-form select:hover, #contact-form textarea:hover {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.4);}
    </style>
	</head>
<body>
        <?php include('layout/header.php'); ?>
		

		<!-- Blog -->
	    <section id="blog" class="blog section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-12">
						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
							<!-- Post Title -->
							<h1 class="wow fadeInUp">Chi siamo</h1>
                            <pdigitagftp2015>Lorem ipsum dolor sit amet consectetur adipiscin elit. Titire tu patulae sub tegmine fagi etiam de gustibus disputandum non est.<br>
                                Maecenas iaculis lorem eget erat lobortis accumsan. Ut rhoncus cursus gravida. Aliquam erat volutpat. Phasellus quis mollis lectus. Ut vehicula ultrices vehicula. Sed egestas tellus eu ante posuere, id mattis nibh faucibus. Ut laoreet sollicitudin mollis. </p>
                            <h2 class="wow fadeInUp">Il team</h2>
												<!-- member -->
					<div class="col-lg-3 col-md-4 col-sm-6 team_member wow fadeInRight" data-wow-duration="700ms" data-wow-delay="500ms">
						<div class="member">
							<div class="member-image">
								<img src="images/team/1.png" alt="Member">
							</div>
							<div class="member-info">
								<h3>Antonio Rossi</h3>
								<span>Cardiologo</span>
								<a href="">Curriculum vitae</a>
                                <p>Tel. 02 123 456<br>
                                <a href="">antonio.rossi@medicinainsieme.it</a></p>							</div>
						</div>
					</div><!-- /member -->

					<!-- member -->
					<div class="col-lg-3 col-md-4 col-sm-6 team_member wow fadeInRight" data-wow-duration="700ms" data-wow-delay="800ms">
						<div class="member">
							<div class="member-image">
								<img src="images/team/2.png" alt="Member">
							</div>
							<div class="member-info">
								<h3>Lucia Verdi</h3>
								<span>Fisioterapista</span>
								<a href="">Curriculum vitae</a>
                                <p>Tel. 02 123 456<br>
                                <a href="">lucia.verdi@medicinainsieme.it</a></p>							</div>
						</div>
					</div><!-- /member -->

					<!-- member -->
					<div class="col-lg-3 col-md-4 col-sm-6 team_member wow fadeInRight" data-wow-duration="700ms" data-wow-delay="1100ms">
						<div class="member">
							<div class="member-image">
								<img src="images/team/3.png" alt="Member">
							</div>
							<div class="member-info">
								<h3>Alberto Neri</h3>
								<span>Medico di base</span>
								<a href="">Curriculum vitae</a>
                                <p>Tel. 02 123 456<br>
                                <a href="">alberto.neri@medicinainsieme.it</a></p>
							</div>
						</div>
					</div><!-- /member -->

					<!-- member -->
					<div class="col-lg-3 col-md-4 col-sm-6 team_member wow fadeInRight" data-wow-duration="700ms" data-wow-delay="1400ms">
						<div class="member">
							<div class="member-image">
								<img src="images/team/4.png" alt="Member">
							</div>
							<div class="member-info">
								<h3>Sara Giallo</h3>
								<span>Oculista</span>
								<a href="">Curriculum vitae</a>
                                <p>Tel. 02 123 456<br>
                                <a href="">sara.giallo@medicinainsieme.it</a></p>
							</div>
						</div>
					</div><!-- /member -->

										<a class="wow fadeInRight" href="acquista-card.php"><button class="btn">Acquista la card!</button></a>
					
					</div><!-- Blog Left Side Ends -->
							
				</div>
			
			</div>
		</section><!-- Our Blog Section Ends -->
		
        <?php include('layout/footer.php'); ?>
		
	</body>
</html>