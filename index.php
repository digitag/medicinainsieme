<!DOCTYPE html>
<html>
	<head>
		<title>MedicinaInsieme</title>
        <?php include('layout/head.php'); ?>
	</head>
<body>
		<!-- header -->
		<header id="header" class="clearfix">
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="container">
					<div class="row">
						<!-- logo -->
						<div class="col-sm-3">
							<div class="navbar-header">
								<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
								</button>
								<a href="#" class="navbar-brand"><img src="images/logo.png" alt="logo">
								</a>

							</div>
						</div><!-- logo -->

						<!-- navbar -->
						<div class="col-sm-9 np">
							<div class="navbar-collapse collapse">
								<ul class="nav navbar-nav">
									<li class="scroll active"><a href="#header">home</a></li>
									<li class="scroll"><a href="#about-our-firm">Chi siamo</a></li>
									<li class="scroll"><a href="#services-offered">Servizi</a></li>
									<li class="scroll"><a href="#how-to-reach-us">Contattaci</a></li>
									<li class="scroll"><a href="login.php">Login</a></li>
								</ul>
							</div>
						</div><!-- /navbar -->
						



					</div>

				</div>
			</div>
		</header><!-- /header -->

		<section id="slider" class="slider">            
					<!-- carousel item -->
					<div class="item">
                    <img src="images/home/1.jpg" width="1920" height="1080">
					<div class="carousel-caption">
						<div class="tb">
								<div class="tb-cell">
									<h1 class="title">Card Salvavita</h1>
									<p>I dati significativi relativi alla tua salute, in un'unica card</p>
									<a href="acquista-card.php"><button class="btn">Acquista ora</button></a>
								</div>
							</div>
						</div>					
					</div><!-- carousel item -->

					<!-- carousel item -->
					<div class="item">
                     <img src="images/home/2.jpg" width="1920" height="1080">
						<div class="carousel-caption">
							<div class="tb">
								<div class="tb-cell">
									<h1 class="title">Un team di medici all'avanguardia</h1>
									<p>I tuoi dati sono in ottime mani!</p>
									<button class="btn"><a href="#">Scopri il servizio</a></button>
								</div>
							</div>
						</div>	
					</div><!-- carousel item -->

					<!-- carousel item -->
					<div class="item">
                     <img src="images/home/3.jpg" width="1920" height="1080">
						<div class="carousel-caption">
							<div class="tb">
								<div class="tb-cell">
									<h1 class="title">Cartella clinica digitale</h1>
									<p>La tua cartella sempre consultabile e aggiornata in tempo reale.</p>
									<button class="btn"><a href="#">Scopri di pi&uacute;</a></button>
								</div>
							</div>
						</div>
					</div><!-- carousel item -->
				
	    </section><!--/#carousel-wrapper--> 
		
		<!-- about-our-firm -->
		<section id="about-our-firm" class="bg-color bg-white"> 
			<div class="container">
				<div class="row">
					<h1 class="title text-center wow zoomIn">Chi siamo</h1>
					<!-- our-skill -->
					<div class="col-lg-6 col-sm-6 col-sm-6 col-xs-12 wow fadeInLeft">
						<div class="our-skill">
							<p>MedicinaInsieme &eacute; una societ&aacute; di servizi che offre a <strong>medici</strong>, <strong>pazienti</strong> e <strong>operatori di salute</strong> una piattaforma digitale in grado di offrire soluzioni di eccellenza.</p>
                            <p>Una di queste soluzioni &eacute; Card Salvavita, che contiene:</p>
    <ul>
        <li>Dati salvavita del paziente</li>
        <li>Cartella clinica digitale</li>
        <li>OliMed</li>
    </ul>

<a href="chi-siamo.php"><button style="margin-top:30px" class="btn">Scopri chi siamo</button></a>
							<!-- skill-bar -->
							<div class="skill-bar" style="margin-top:20px">
								<div class="animation fadeInUp">
									<div class="h-default-themed">
										<label>Errori causati da diagnosi errate, mancanti o ritardate</label>
										<div class="progress"> 
											<div class="bar progress-bar">52%</div>
										</div>
										
										<label>tempo risparmiato per le diagnosi con la condivisione delle informazioni fra i medici</label>
										<div class="progress">
											<div class="bar progress-bar">35%</div>
										</div>
										
										
									</div>
								</div>
							</div><!-- skill-bar -->
						</div>
					</div><!-- our-skill -->

					<!-- ceo-message -->
					<div class="col-lg-6 col-sm-6 col-sm-6 col-xs-12 wow fadeInRight">
						<div class="ceo-message">
							<img src="images/ceo-message.jpg" alt="CEO Measege" width="569" height="456">
							<div class="ceo-overlay">
							  <div class="message-content col-lg-12">
									<p>I Manager della tua salute!</p>
								</div>
							</div>
						</div>
					</div><!-- ceo-message -->
				</div> 
			</div>
		</section><!-- /about-our-firm -->
		
		<!-- case-resolving -->
		<section id="case-resolving" class="bg-image bg-green">
			<div class="container">
				<div class="row">
					<div class="col-lg-12 text-center">
						<h1 class="wow fadeInLeft" data-wow-duration="700ms" data-wow-delay="700ms">Vuoi entrare a far parte di MedicinaInsieme?</h1>
                        <button style="margin:20px 0" class="btn btn-transparent wow fadeInUp" data-wow-duration="700ms" data-wow-delay="700ms"><a href="acquista-card.php">Acquista la card</a></button>
					</div>

					<div class="col-lg-12 text-center">
						<h2 class="wow fadeInLeft" data-wow-duration="700ms" data-wow-delay="700ms">Un mondo di vantaggi per te!</h2>
						<ul class="wow fadeInRight" data-wow-duration="700ms" data-wow-delay="700ms">
                        <li>Tutti i tuoi dati sempre aggiornati</li>
                        <li>Semplificazione delle operazioni di primo soccorso</li>
                        <li>Diagnosi a 360&deg;</li>
                        <li>Un network di medici e specialisti a tua disposizione</li>
                        </ul>
					</div>

				</div>
			</div>
		</section><!-- /case-resolving -->
		
		<!-- services-offered -->
		<section id="services-offered" class="text-center">
			<div class="container">
				<div class="row">
					<h1 class="title wow zoomIn">Servizi</h1>

					<!-- single-service -->
					<div class="col-lg-4 col-sm-4 col-sm-4 col-xs-12 wow fadeInRight" data-wow-duration="700ms" data-wow-delay="500ms">
						<div class="single-service">
							<a href="card-salvavita.php"><i class="fa fa-heart"></i></a>
							<h3><a href="card-salvavita.php">Card Salvavita</a></h3>
							<p>La tua <strong>Card Salvavita</strong> contiene i dati significativi relativi alla tua salute utili per il <strong>primo soccorso</strong>.</p>
                            <a href="card-salvavita.php">Scopri di pi&uacute;</a>
						</div>
					</div><!-- single-service -->

					<!-- single-service -->
					<div class="col-lg-4 col-sm-4 col-sm-4 col-xs-12 wow fadeInRight" data-wow-duration="700ms" data-wow-delay="500ms">
						<div class="single-service">
							<a href="cartella-clinica-digitale.php"><i class="fa fa-stethoscope"></i></a>
							<h3><a href="cartella-clinica-digitale.php">Cartella clinica digitale</a></h3>
							<p>La tua <strong>cartella clinica digitale</strong> sar&aacute; aggioranta dai medici di MedicinaInsieme a seguito di ogni visita medica.</p>
                            <a href="cartella-clinica-digitale.php">Scopri di pi&uacute;</a>
						</div>
					</div><!-- single-service -->

					<!-- single-service -->
					<div class="col-lg-4 col-sm-4 col-sm-4 col-xs-12 wow fadeInRight" data-wow-duration="700ms" data-wow-delay="500ms">
						<div class="single-service">
							<a href="olimed.php"><i class="fa fa-globe"></i></a>
							<h3><a href="olimed.php">OliMed</a></h3>
							<p>La tua cartella clinica potr&aacute; essere <strong>condivisa</strong> fra i medici di MedicinaInsieme con lo scopo di <strong>migliorare le tue diagnosi</strong>.</p>
                            <a href="olimed.php">Scopri di pi&uacute;</a>
						</div>
					</div><!-- single-service -->

				</div>
			</div>
		</section><!-- /services-offered -->
		

		<!-- how-to-reach-us -->
		<section id="how-to-reach-us" class="bg-image text-center bg-mirage-rgba">
			<div class="container">
				<div class="row">
					
					<div class="col-lg-12 col-sm-12 col-sm-12 col-xs-12 wow fadeInUp" data-wow-duration="700ms" data-wow-delay="300ms">
						<!-- contact-form --><h1 class="title wow fadeInDown" data-wow-duration="700ms" data-wow-delay="300ms">Contattaci</h1>
						<form id="contact-form" class="contact-form" name="contact-form" method="" action="">
                        <h1 class="show-on-success lead" style="display:none;">Thanks for the mail, We will contact you shortly</h1>
							<div class="row">
								<div class="form-group col-sm-6 name-field">
					                <input type="text" id="name" name="name" class="form-control" required placeholder="Nome">
					            </div>
                                
                                <div class="form-group col-sm-6 email-field">
					                <input type="email" id="email" name="email" class="form-control" required placeholder="Indirizzo Email">
					            </div>

					            <div class="form-group col-sm-12 subject-field">
				                     <input type="text" id="subject" name="subject" class="form-control" required placeholder="Oggetto">
					            </div>
					            <div class="form-group col-sm-12">
					                <textarea name="message" id="message" required class="form-control" rows="8" placeholder="Lascia il tuo messaggio"></textarea>
					            </div> 
							</div>				                                   
				            <div class="form-group">
       						<button type="submit" class="btn wow fadeInRight">Invia</button>
				            </div>
				        </form>	<!-- contact-form -->
					</div>
				</div>
			</div>
		</section><!-- how-to-reach-us -->

		<!-- map-wrapper -->
		<section id="map-wrapper">
	</section><!-- map-wrapper -->

        <?php include('layout/footer.php'); ?>
        
        <script src="js/jquery.bxslider.min.js"></script>  <!-- Bx Slider -->
        <script src="http://maps.google.com/maps/api/js?sensor=true"></script><!-- Map Api -->
		<script src="js/gmaps.js"></script>  <!-- Gmap JS -->
        <script src="js/script-home.js"></script>  <!-- Script JS -->
		
	</body>
</html>