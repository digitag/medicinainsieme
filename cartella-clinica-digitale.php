<!DOCTYPE html>
<html>
	<head>
		<title>Cartella Clinica MedicinaInsieme</title>
        <?php include('layout/head.php'); ?>
	</head>
<body>
        <?php include('layout/header.php'); ?>
		

		<!-- Blog -->
	    <section id="blog" class="blog section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-12">
 
    
    
    
    
    						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
							<!-- Post Title -->
							<h2 class="wow fadeInUp">Cartella Clinica Digitale</h2>
							<div class="post wow fadeInUp">
								<div class="post-content">	
									<!-- Text -->
                                    <p>Grazie alla propria card il paziente avrá accesso alla propria <strong>cartella clinica digitale</strong>, che sará consultabile in qualsiasi momento.</p>
									<p> Attraverso il login personalizzato un medico del network MedicinaInsieme avrá la possibilitá di creare la cartella clinica digitale del paziente e <strong>aggiornarla</strong> a seguito di ogni visita medica, inserendo all'interno del profilo eventuali esami clinici, referti e terapie seguite. </p>
								</div>
                                
                                <img class="img-responsive" src="images/cartella-clinica.jpg" alt="cartella clinica">
                                
							</div>
						</div><!-- End Post -->

    
    
    					
						
					</div><!-- Blog Left Side Ends -->

				</div>
			
			</div>
		</section><!-- Our Blog Section Ends -->
		

        <?php include('layout/footer.php'); ?>
		
	</body>
</html>