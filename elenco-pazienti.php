
<!DOCTYPE html>
<html>
	<head>
		<title>Elenco pazienti MedicinaInsieme</title>
        <?php include('layout/head.php'); ?>
	<style>
	#contact-form input, #contact-form select, #contact-form textarea {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.2);}
	#contact-form input:hover, #contact-form select:hover, #contact-form textarea:hover {
    background: none repeat scroll 0 0 rgba(0, 0, 0, 0.4);}
	#referti a{display:block;color:#333;text-decoration:underline}
	#referti a:hover{color:#777;text-decoration:none}
	.grey-bg{background-color:#ededed}
	.glyphicon-search{padding-top:6px;cursor:pointer}
    </style>
	</head>
<body>
        <?php include('layout/header.php'); ?>
		

		<!-- Blog -->
	    <section id="blog" class="blog section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-12">
						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
							<!-- Post Title -->
							<h1 class="wow fadeInUp">Benvenuto, Dottor Bianchi</h1>
							<div class="post wow fadeInUp">
								<div class="post-content">
							<h2 class="wow fadeInUp">Elenco pazienti</h2>
									<!-- Text -->
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            <strong>NOME</strong>
                                            </div>
                                            <div class="col-md-3">
                                            <strong>DATA DI NASCITA</strong>
                                            </div>
                                            <div class="col-md-3">
                                            <strong>CITT&Aacute;</strong>
                                            </div>
                                        </div>
                                        <div class="col-md-12 grey-bg">
                                            <div class="col-md-6">
                                            Verdi Francesco
                                            </div>
                                            <div class="col-md-3">
                                            27/12/1987
                                            </div>
                                            <div class="col-md-3">
                                            Milano <a href="scheda-paziente.php"><span class="glyphicon glyphicon-search pull-right"></span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Rossi Antonella
                                            </div>
                                            <div class="col-md-3">
                                            21/01/1990
                                            </div>
                                            <div class="col-md-3">
                                            Monza <a href="scheda-paziente.php"><span class="glyphicon glyphicon-search pull-right"></span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 grey-bg">
                                            <div class="col-md-6">
                                            Neri Cristina
                                            </div>
                                            <div class="col-md-3">
                                            18/02/1958
                                            </div>
                                            <div class="col-md-3">
                                            Agrate Brianza <a href="scheda-paziente.php"><span class="glyphicon glyphicon-search pull-right"></span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Bianco Enrico
                                            </div>
                                            <div class="col-md-3">
                                            05/10/1956
                                            </div>
                                            <div class="col-md-3">
                                            Cusano Milanino <a href="scheda-paziente.php"><span class="glyphicon glyphicon-search pull-right"></span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 grey-bg">
                                            <div class="col-md-6">
                                            Verdi Francesco
                                            </div>
                                            <div class="col-md-3">
                                            27/12/1987
                                            </div>
                                            <div class="col-md-3">
                                            Milano <a href="scheda-paziente.php"><span class="glyphicon glyphicon-search pull-right"></span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Rossi Antonella
                                            </div>
                                            <div class="col-md-3">
                                            21/01/1990
                                            </div>
                                            <div class="col-md-3">
                                            Monza <a href="scheda-paziente.php"><span class="glyphicon glyphicon-search pull-right"></span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12 grey-bg">
                                            <div class="col-md-6">
                                            Neri Cristina
                                            </div>
                                            <div class="col-md-3">
                                            18/02/1958
                                            </div>
                                            <div class="col-md-3">
                                            Agrate Brianza <a href="scheda-paziente.php"><span class="glyphicon glyphicon-search pull-right"></span></a>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="col-md-6">
                                            Bianco Enrico
                                            </div>
                                            <div class="col-md-3">
                                            05/10/1956
                                            </div>
                                            <div class="col-md-3">
                                            Cusano Milanino <a href="scheda-paziente.php"><span class="glyphicon glyphicon-search pull-right"></span></a>                                            </div>
                                        </div>
                                    
                                    </div>
								</div>
							</div>
						</div><!-- End Post -->
						
                        
					</div><!-- Blog Left Side Ends -->
							
				</div>
			
			</div>
		</section><!-- Our Blog Section Ends -->
		

        <?php include('layout/footer.php'); ?>
		
	</body>
</html>