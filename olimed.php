<!DOCTYPE html>
<html>
	<head>
		<title>OliMed MedicinaInsieme</title>
        <?php include('layout/head.php'); ?>
	</head>
<body>
        <?php include('layout/header.php'); ?>
		

		<!-- Blog -->
	    <section id="blog" class="blog section">
			<div class="container">
				<div class="row">
					<!-- Blog Left Side Begins -->
					<div class="col-md-12">
						<!-- Post -->
						<div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
							<!-- Post Title -->
							<h2 class="wow fadeInUp">Olimed</h2>
							<div class="post wow fadeInUp">
								<!-- Image -->
								<div class="post-content">	
									<!-- Text -->
							<p>La card di MedicinaInsieme permette di <strong>agevolare la diagnosi</strong> e la cura del paziente grazie alla <strong>condivisione delle informazioni</strong> e dei dati relativi al paziente tra i medici presso cui &eacute; in cura. </p>
                            
<p>Un medico appartenente al network, infatti, ha la possibilit&aacute; di invitare altri medici del network a consultare la cartella clinica online prima di visitare il paziente stesso con lo scopo di <strong>migliorare la diagnosi al paziente</strong>. </p>
								</div>
<img class="img-responsive" style="width:100%" src="images/olimed.jpg" alt="OliMed">
							</div>
						</div><!-- End Post -->
						
    					
						
					</div><!-- Blog Left Side Ends -->

				</div>
			
			</div>
		</section><!-- Our Blog Section Ends -->
		

        <?php include('layout/footer.php'); ?>
		
	</body>
</html>