<!DOCTYPE html>
<html>

<head>
    <title>Login MedicinaInsieme</title>
    <?php include('layout/head.php'); ?>
        <style>
            #contact-form input,
            #contact-form select,
            #contact-form textarea {
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.2);
            }
            
            #contact-form input:hover,
            #contact-form select:hover,
            #contact-form textarea:hover {
                background: none repeat scroll 0 0 rgba(0, 0, 0, 0.4);
            }

        </style>
</head>

<body>
    <?php include('layout/header.php'); ?>


        <!-- Blog -->
        <section id="blog" class="blog section">
            <div class="container">
                <div class="row">
                    <!-- Blog Left Side Begins -->
                    <div class="col-md-12">
                        <!-- Post -->
                        <div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
                            <!-- Post Title -->
                            <h2 class="wow fadeInUp">Login</h2>
                            <p>Inserisci i tuoi dati per accedere alla piattaforma MedicinaInsieme</p>
                            <div class="post wow fadeInUp">
                                <div class="post-content">
                                    <!-- Text -->
                                    <form id="contact-form" class="contact-form" name="contact-form" method="" action="">
                                        <h1 class="show-on-success lead" style="display:none;">Thanks for the mail, We will contact you shortly</h1>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <h2>Paziente</h2>
                                                <div class="clearfix"></div>

                                                <div class="form-group col-sm-6 name-field">
                                                    <input type="text" id="name" name="name" class="form-control" required placeholder="Numero Card">
                                                </div>

                                                <div class="form-group col-sm-6 email-field">
                                                    <input type="email" id="email" name="email" class="form-control" required placeholder="Password">
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <h2>Medico</h2>
                                                <div class="clearfix"></div>

                                                <div class="form-group col-sm-6 name-field">
                                                    <input type="text" id="name" name="name" class="form-control" required placeholder="Username">
                                                </div>

                                                <div class="form-group col-sm-6 email-field">
                                                    <input type="email" id="email" name="email" class="form-control" required placeholder="Password">
                                                </div>
                                            </div>

                                        </div>

                                        <p>Hai smarrito username o password? Mandaci una <a href="#">mail</a> o contattaci al numero 012 345 67 89</p>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <button onclick="window.location='modifica-dati.php'" class="btn wow fadeInRight">Entra</button>
                                                </div>
                                            </div>
                                            <div class="col-md-6">

                                                <div class="form-group">
                                                    <button onclick="window.location='elenco-pazienti.php'" class="btn wow fadeInRight">Entra</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>

                                </div>
                            </div>
                        </div>
                        <!-- End Post -->

                        <!-- Post -->
                        <div class="post-item wow" data-animation="fadeInUp" data-animation-delay="300">
                            <!-- Post Title -->
                            <h2 class="wow fadeInUp">Registrazione</h2>
                            <div class="post wow fadeInUp">
                                <div class="post-content">
                                    <!-- Text -->
                                    <p>Attiva la tua card registrandoti su MedicinaInsieme</p>
                                    <form id="contact-form" class="contact-form" name="contact-form" method="" action="">
                                        <h1 class="show-on-success lead" style="display:none;">Thanks for the mail, We will contact you shortly</h1>
                                        <div class="row">
                                            <div class="form-group col-sm-6 name-field">
                                                <input type="text" id="name" name="name" class="form-control" required placeholder="Numero card">
                                            </div>
                                            <div class="form-group col-sm-6 name-field">
                                                <input type="text" id="name" name="name" class="form-control" required placeholder="Password">
                                            </div>
                                            <div class="form-group col-sm-6 name-field">
                                                <input type="text" id="name" name="name" class="form-control" required placeholder="Il tuo nome">
                                            </div>
                                            <div class="form-group col-sm-6 name-field">
                                                <input type="text" id="name" name="name" class="form-control" required placeholder="Il tuo cognome">
                                            </div>

                                            <div class="form-group col-sm-6 email-field">
                                                <input type="email" id="email" name="email" class="form-control" required placeholder="Indirizzo Email">
                                            </div>

                                            <div class="form-group col-sm-6 subject-field">
                                                <input type="text" id="subject" name="subject" class="form-control" required placeholder="Codice fiscale">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <button type="submit" class="btn wow fadeInRight">Invia</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- End Post -->

                    </div>
                    <!-- Blog Left Side Ends -->

                </div>

            </div>
        </section>
        <!-- Our Blog Section Ends -->


        <?php include('layout/footer.php'); ?>

</body>

</html>
